/*
 * OCLEnum.h
 *
 *  Created on: 25 Jan 2012
 *      Author: v1smoll
 */

#ifndef OCLENUM_H_
#define OCLENUM_H_

#include <llvm/Value.h>

namespace axtor {

	// returns the string representation of a mem_fence..enum generated by the OpenCL Frontend
	bool evaluateEnum_MemFence(llvm::Value * value, std::string & result);

}

#endif /* OCLENUM_H_ */
